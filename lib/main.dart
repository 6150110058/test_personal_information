import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_personal_information/providers/data_provider.dart';
import 'package:test_personal_information/screens/home_screen.dart';

void main() {
  runApp(ChangeNotifierProvider(
      create: (_) => DataProvider(), child: const MyApp()));
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Test Personal Information',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: const HomeScreen(),
    );
  }
}
