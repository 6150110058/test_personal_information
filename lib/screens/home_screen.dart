import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:test_personal_information/providers/data_provider.dart';
import 'package:test_personal_information/screens/add_from1.dart';
import 'package:test_personal_information/screens/information_detail.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({
    Key? key,
  }) : super(key: key);

  @override
  State<HomeScreen> createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        title: Center(
          child: Text('แอพพลิเคชันเก็บข้อมูลบุคคล',
              style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500, fontFamily: "Prompt")),
        ),
      ),
      body: SafeArea(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 25),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              const Text('รายชื่อทั้งหมด',
                  style: TextStyle(
                      fontSize: 24,
                      color: Colors.black,
                      fontWeight: FontWeight.w500,
                      fontFamily: "Prompt")),
              SizedBox(
                height: 25,
              ),
              Container(
                  child: Provider.of<DataProvider>(context).name.isEmpty
                      ? Row(
                          children: [
                            Container(
                                child: Text('กรุณาเพิ่มข้อมูล',
                                    style: TextStyle(
                                        fontSize: 18,
                                        color: Colors.grey,
                                        fontWeight: FontWeight.w400,
                                        fontFamily: "Prompt"))),
                            SizedBox(
                              width: 10,
                            ),
                            Icon(Icons.add_circle, color: Colors.blue)
                          ],
                        )
                      : InkWell(
                          onTap: () {
                            Navigator.push(context,
                                MaterialPageRoute(builder: (context) => InformationDetail()));
                          },
                          child: CardPerson())),
            ],
          ),
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.push(context, MaterialPageRoute(builder: (context) => AddFrom1()));
        },
        tooltip: 'เพิ่มข้อมูล',
        child: const Icon(Icons.add),
      ),
    );
  }

  Widget CardPerson() {
    return Container(
      decoration: BoxDecoration(
        border: Border.all(color: Colors.blue),
        borderRadius: BorderRadius.circular(8),
        color: Colors.white,
        boxShadow: [
          BoxShadow(
            color: Colors.black12,
            spreadRadius: 1,
            blurRadius: 1,
            offset: Offset(0, 1), // changes position of shadow
          ),
        ],
      ),
      height: 200,
      child: Padding(
        padding: const EdgeInsets.all(15.0),
        child: Column(
          children: [
            Row(
              children: [
                Text('ชื่อ : ',
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                        fontFamily: "Prompt")),
                Text(
                  Provider.of<DataProvider>(context).name,
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.w400,
                      fontFamily: "Prompt"),
                ),
                SizedBox(
                  width: 5,
                ),
                Text(
                  Provider.of<DataProvider>(context).surname,
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.w400,
                      fontFamily: "Prompt"),
                ),
              ],
            ),
            SizedBox(
              height: 15,
            ),
            Row(
              children: [
                Text('อายุ : ',
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                        fontFamily: "Prompt")),
                Text(
                  Provider.of<DataProvider>(context).age,
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.w400,
                      fontFamily: "Prompt"),
                ),
              ],
            ),
            SizedBox(
              height: 15,
            ),
            Row(
              children: [
                Text('เบอร์โทรศัพท์ : ',
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                        fontFamily: "Prompt")),
                Text(
                  Provider.of<DataProvider>(context).tel,
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.w400,
                      fontFamily: "Prompt"),
                ),
              ],
            ),
            SizedBox(
              height: 15,
            ),
            Row(
              children: [
                Text('วันเกิด : ',
                    style: TextStyle(
                        fontSize: 18,
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                        fontFamily: "Prompt")),
                Text(
                  Provider.of<DataProvider>(context).birthdate,
                  style: TextStyle(
                      fontSize: 18,
                      color: Colors.grey,
                      fontWeight: FontWeight.w400,
                      fontFamily: "Prompt"),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
