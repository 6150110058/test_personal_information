import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:provider/provider.dart';
import 'package:test_personal_information/providers/data_provider.dart';

import 'add_from2.dart';

class AddFrom1 extends StatefulWidget {
  const AddFrom1({Key? key}) : super(key: key);

  @override
  _AddFrom1State createState() => _AddFrom1State();
}

class _AddFrom1State extends State<AddFrom1> {
  final fromKey = GlobalKey<FormState>();

  final _name = TextEditingController();
  final _surname = TextEditingController();
  final _nickname = TextEditingController();
  final _age = TextEditingController();
  final _tel = TextEditingController();
  String _sex = 'ชาย';
  int _value = 1;

  var maskFormatPhoneNum =
      new MaskTextInputFormatter(mask: '##########', filter: {"#": RegExp(r'[0-9]')});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
            title: Text('เพิ่มข้อมูล',
                style: TextStyle(fontSize: 18, fontWeight: FontWeight.w500, fontFamily: "Prompt"))),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 25),
          child: Form(
            key: fromKey,
            child: ListView(
              // mainAxisAlignment: MainAxisAlignment.start,
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Text(
                    'ข้อมูลส่วนตัว',
                    style: TextStyle(
                        fontSize: 24,
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                        fontFamily: "Prompt"),
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                Container(
                  child: TextFormField(
                    controller: _name,
                    decoration: InputDecoration(
                      labelText: "ชื่อ",
                      labelStyle:
                          TextStyle(fontSize: 16, color: Colors.black, fontFamily: "Prompt"),
                      border: OutlineInputBorder(),
                    ),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    onChanged: (value) {},
                    validator: (value) {
                      if (_name.text.isEmpty) {
                        return 'กรุณาระบุ';
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  child: TextFormField(
                    controller: _surname,
                    decoration: InputDecoration(
                      labelText: "นามสกุล",
                      labelStyle:
                          TextStyle(fontSize: 16, color: Colors.black, fontFamily: "Prompt"),
                      border: OutlineInputBorder(),
                    ),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (value) {
                      if (_surname.text.isEmpty) {
                        return 'กรุณาระบุ';
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  child: TextFormField(
                    controller: _nickname,
                    decoration: InputDecoration(
                      labelText: "ชื่อเล่น",
                      labelStyle:
                          TextStyle(fontSize: 16, color: Colors.black, fontFamily: "Prompt"),
                      border: OutlineInputBorder(),
                    ),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (value) {
                      if (_nickname.text.isEmpty) {
                        return 'กรุณาระบุ';
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  child: TextFormField(
                      controller: _age,
                      keyboardType: TextInputType.number,
                      decoration: InputDecoration(
                        labelText: "อายุ",
                        labelStyle:
                            TextStyle(fontSize: 16, color: Colors.black, fontFamily: "Prompt"),
                        border: OutlineInputBorder(),
                      ),
                      autovalidateMode: AutovalidateMode.onUserInteraction,
                      validator: (value) {
                        if (_age.text.isEmpty) {
                          return 'กรุณาระบุ';
                        } else {
                          return null;
                        }
                      }),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  child: TextFormField(
                    controller: _tel,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      labelText: "เบอร์โทรศัพท์",
                      labelStyle:
                          TextStyle(fontSize: 16, color: Colors.black, fontFamily: "Prompt"),
                      border: OutlineInputBorder(),
                    ),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    inputFormatters: [maskFormatPhoneNum],
                    validator: (value) {
                      if (_tel.text.isEmpty) {
                        return 'กรุณาระบุ';
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text('เพศ',
                          style: TextStyle(
                              fontSize: 16,
                              color: Colors.black,
                              fontWeight: FontWeight.w500,
                              fontFamily: "Prompt")),
                      Row(
                        children: [
                          Radio(
                            value: 1,
                            groupValue: _value,
                            onChanged: (int? value) {
                              setState(() {
                                _value = value!;
                                _sex = 'ชาย';
                              });
                            },
                          ),
                          SizedBox(width: 10),
                          Text('ชาย',
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Prompt"))
                        ],
                      ),
                      Row(
                        children: [
                          Radio(
                            value: 2,
                            groupValue: _value,
                            onChanged: (int? value) {
                              setState(() {
                                _value = value!;
                                _sex = 'หญิง';
                              });
                            },
                          ),
                          SizedBox(width: 10),
                          Text('หญิง',
                              style: TextStyle(
                                  fontSize: 16,
                                  color: Colors.black,
                                  fontWeight: FontWeight.w400,
                                  fontFamily: "Prompt"))
                        ],
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  height: 48,
                  child: ElevatedButton(
                    onPressed: () {
                      if (_name.text.isNotEmpty &&
                          _surname.text.isNotEmpty &&
                          _nickname.text.isNotEmpty &&
                          _age.text.isNotEmpty &&
                          _tel.text.isNotEmpty &&
                          _sex.isNotEmpty) {
                        Provider.of<DataProvider>(context, listen: false).inputDataPersonal(
                            _name.text, _surname.text, _nickname.text, _age.text, _tel.text, _sex);
                        Navigator.push(
                            context, MaterialPageRoute(builder: (context) => AddFrom2()));
                      } else {
                        fromKey.currentState!.validate();
                      }
                    },
                    child: Text('ถัดไป',
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.w500, fontFamily: "Prompt")),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
