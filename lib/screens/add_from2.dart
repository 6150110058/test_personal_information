import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mask_text_input_formatter/mask_text_input_formatter.dart';
import 'package:provider/provider.dart';
import 'package:test_personal_information/providers/data_provider.dart';
import 'package:test_personal_information/screens/information_detail.dart';

class AddFrom2 extends StatefulWidget {
  const AddFrom2({Key? key}) : super(key: key);

  @override
  _AddFrom2State createState() => _AddFrom2State();
}

class _AddFrom2State extends State<AddFrom2> {
  final fromKey = GlobalKey<FormState>();

  final _idNumber = TextEditingController();
  final _birthdate = TextEditingController();
  final _address = TextEditingController();

  final items = ['พุทธ', 'อิสลาม', 'คริสต์'];
  String _religion = 'พุทธ';

  String? day = '';
  String? month = '';
  String? year = '';

  var maskFormatIdentification = new MaskTextInputFormatter(
      mask: '# #### ##### ## #', filter: {"#": RegExp(r'[0-9]')});
  var maskFormatDate = new MaskTextInputFormatter(
      mask: '##/##/####', filter: {"#": RegExp(r'[0-9]')});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        appBar: AppBar(
            title: Text('เพิ่มข้อมูล',
                style: TextStyle(
                    fontSize: 18,
                    fontWeight: FontWeight.w500,
                    fontFamily: "Prompt"))),
        body: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 25),
          child: Form(
            key: fromKey,
            child: ListView(
              // mainAxisAlignment: MainAxisAlignment.start,
              // crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  child: Text(
                    'ข้อมูลตามบัตรประชาชน',
                    style: TextStyle(
                        fontSize: 24,
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                        fontFamily: "Prompt"),
                  ),
                ),
                SizedBox(
                  height: 25,
                ),
                Container(
                  child: TextFormField(
                    controller: _idNumber,
                    keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      labelText: "เลขประจำตัวประชาชน",
                      labelStyle: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                          fontFamily: "Prompt"),
                      border: OutlineInputBorder(),
                    ),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    inputFormatters: [maskFormatIdentification],
                    validator: (value) {
                      if (_idNumber.text.isEmpty) {
                        return 'กรุณาระบุ';
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  child: TextFormField(
                    controller: _birthdate,
                    // keyboardType: TextInputType.number,
                    decoration: InputDecoration(
                      labelText: "วันเกิด",
                      labelStyle: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                          fontFamily: "Prompt"),
                      border: OutlineInputBorder(),
                      suffixIcon: InkWell(
                        child: Icon(
                          Icons.calendar_today_outlined,
                          size: 13,
                        ),
                        onTap: () async {
                          await showDatePicker(
                            context: context,
                            initialDate: DateTime.now(),
                            firstDate: DateTime(1000),
                            lastDate: DateTime.now(),
                          ).then((value) => {
                                if (value != null)
                                  {
                                    _birthdate.text =
                                        DateFormat("dd/MM/yyyy").format(value),
                                    year = value.year.toString(),
                                    day = value.day.toString(),
                                    month = value.month.toString(),
                                  }
                              });
                        },
                      ),
                    ),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    inputFormatters: [maskFormatDate],
                    onChanged: (value) {
                      if (value.length == 10) {
                        day = value.toString().substring(0, 2);
                        month = value.toString().substring(3, 5);
                        year = value.toString().substring(6, 10);
                        if (int.parse(year!) > 3000 ||
                            int.parse(month!) > 12 ||
                            int.parse(day!) > 31) {
                          day = '';
                          month = '';
                          year = '';
                          _birthdate.text = '';
                        } else {
                          _birthdate.text = day! + '/' + month! + '/' + year!;
                        }
                      }
                    },
                    validator: (value) {
                      if (_birthdate.text.isEmpty) {
                        return 'กรุณาระบุ';
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  child: TextFormField(
                    maxLines: 3,
                    controller: _address,
                    decoration: InputDecoration(
                      labelText: "ที่อยู่",
                      labelStyle: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                          fontFamily: "Prompt"),
                      border: OutlineInputBorder(),
                    ),
                    autovalidateMode: AutovalidateMode.onUserInteraction,
                    validator: (value) {
                      if (_address.text.isEmpty) {
                        return 'กรุณาระบุ';
                      } else {
                        return null;
                      }
                    },
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Container(
                  child: Text(
                    'ศาสนา',
                    style: TextStyle(
                        fontSize: 16,
                        color: Colors.black,
                        fontWeight: FontWeight.w500,
                        fontFamily: "Prompt"),
                  ),
                ),
                buildReligion(),
                SizedBox(
                  height: 15,
                ),
                Container(
                  height: 48,
                  child: ElevatedButton(
                    onPressed: () {
                      if (_idNumber.text.isNotEmpty &&
                          _birthdate.text.isNotEmpty &&
                          _address.text.isNotEmpty &&
                          _religion.isNotEmpty) {
                        Provider.of<DataProvider>(context, listen: false)
                            .inputDataID(_idNumber.text, _birthdate.text,
                                _address.text, _religion);
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                                builder: (context) => InformationDetail()));
                      } else {
                        fromKey.currentState!.validate();
                      }
                    },
                    child: Text('บันทึก',
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                            fontFamily: "Prompt")),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget buildReligion() {
    return Center(
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 10),
        decoration: BoxDecoration(
          border: Border.all(color: Colors.black38),
          borderRadius: BorderRadius.circular(8),
        ),
        child: DropdownButtonHideUnderline(
          child: DropdownButton<String>(
            isExpanded: true,
            borderRadius: BorderRadius.circular(8),
            value: _religion,
            icon: Icon(Icons.arrow_drop_down),
            items: items.map(buildMenuTtem).toList(),
            onChanged: (value) => setState(() => {
                  this._religion = value!,
                }),
          ),
        ),
      ),
    );
  }

  DropdownMenuItem<String> buildMenuTtem(String item) {
    return DropdownMenuItem(
      value: item,
      child: Text(item,
          style: TextStyle(
              fontSize: 16,
              color: Colors.black,
              fontWeight: FontWeight.w400,
              fontFamily: "Prompt")),
    );
  }
}
