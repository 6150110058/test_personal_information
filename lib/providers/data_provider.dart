import 'package:flutter/material.dart';

class DataProvider extends ChangeNotifier {
  String name = '';
  String surname = '';
  String nickname = '';
  String age = '';
  String tel = '';
  String sex = '';
  String idNumber = '';
  String birthdate = '';
  String address = '';
  String religion = '';

  void inputDataPersonal(_name, _surname, _nickname, _age, _tel, _sex) {
    this.name = _name;
    this.surname = _surname;
    this.nickname = _nickname;
    this.age = _age;
    this.tel = _tel;
    this.sex = _sex;
    notifyListeners();
  }

  void inputDataID(_idNumber, _birthdate, _address, _religion) {
    this.idNumber = _idNumber;
    this.birthdate = _birthdate;
    this.address = _address;
    this.religion = _religion;

    notifyListeners();
  }
}
